Azure Infrastructure Deployment Script
Project Description

This project contains a Bash script using Azure CLI for deploying infrastructure on Microsoft Azure. The script is designed to handle different environments like production, test, or development. It adheres to a specific naming convention and manages resources in a highly structured manner.

<img src="./schema.png" align="center" width="600" alt="Project icon">

Prerequisites

    Azure CLI installed
    Bash environment
    SSH keys generated locally

Setup and Usage

    Clone this repository.
    Ensure that you are logged into Azure CLI.
    To deploy the infrastructure, run ./manageVMs.sh [environment]. Replace [environment] with prod, test, or dev as required.
    To delete the resource group, run .removeGroup.sh script with the desired environment parameter.

SSH Configuration

    Authentication is done exclusively through SSH keys.
    A single pair of SSH keys is used for accessing all VMs through the bastion host using SSH agent forwarding.

Optional Features

    The script can also deploy PaaS resources like Blob Storage or Azure SQL Server, which are not integrated into the VNet.

Contribution

To contribute to this project, please follow the standard coding practices and commit message guidelines. For detailed information on contributing, refer to the contribution guide.
Licensing

This project is released under [specify the license], which allows for modification and redistribution under certain conditions.
Contact

For any inquiries or contributions, please contact joseph.laroche.but3-2324si@greta-0726.fr and emerick.roman.but3-2324si@greta-0726.fr.
