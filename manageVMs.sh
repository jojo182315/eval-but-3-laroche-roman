#!/bin/bash

# Program made by Emerick ROMAN and Joseph LAROCHE
# Free from any copyright

# This script intends to manage all different functions and environment variables

# Variables are stored here
source .env

# Functions here
source functions.sh

echo "Hello on manageVMs !"
echo ""

# Check for existing SSH keys and delete if they exist
if [ -f "eval_key" ] || [ -f "eval_key.pub" ]; then
    echo "Deleting old SSH keys..."
    rm -f eval_key eval_key.pub
fi

# Generate SSH key pair
echo "Generating SSH key pair..."
ssh-keygen -t rsa -b 2048 -f eval_key -N ""

# Set appropriate permissions
echo "Setting permissions for the keys..."
chmod 600 eval_key
chmod 644 eval_key.pub

echo "SSH keys have been generated and configured with the appropriate permissions."

# Checking if the current group exists
echo "Checking if the group exists..."
check_resource_group_exists "$RG_NAME"

RESULT=$?

# If the resource group exists, we abort everything
echo ""
if [ $RESULT -eq 0 ]
then
    echo "Resource group already exists"
    echo "Aborting..."
    exit 1
else
    echo "Resource group does not exist... creating..."
    create_resource_group "$RG_NAME" "$LOCATION"
fi

# Checking if the creation is a success
if [ $? -eq 0 ]
then
    echo "An error occured during creation of the resource group"
    exit 1
else
    echo "Resource group successfully created"
fi

# Creating vNet
echo "Creating vNet..."
create_vnet "$VNET_NAME" "$RG_NAME" "$NETWORK" "$SUBNETWORK" "$SUBNET_NAME"

# Checking if the creation is a success
if [ $? -eq 0 ]
then
    echo "An error occured during creation of the vNet"
    exit 1
else
    echo "vNet successfully created"
fi

# Creating Public ip object
create_public_ip "$RG_NAME" "$NAME_PUBLIC_IP" "$LOCATION"

# Checking if the creation is a success
if [ $? -eq 0 ]
then
    echo "An error occured during creation of the public IP"
    exit 1
else
    echo "Public IP successfully created"
fi

# Creating NIC object
create_nic "$RG_NAME" "$NAME_NIC" "$VNET_NAME" "$SUBNET_NAME" "$NAME_PUBLIC_IP" "$PRIVATE_IP_BASTION"

# Checking if the creation is a success
if [ $? -eq 0 ]
then
    echo "An error occured during creation of the NIC"
    exit 1
else
    echo "NIC successfully created"
fi

# Creating bastion

create_bastion "$RG_NAME" "$BASTION_NAME" "$VM_IMAGE" "$ADMIN_USERNAME" "$SSH_PUBLIC_KEY" "$NAME_NIC" "$SIZE"

# Checking if the creation is a success
if [ $? -eq 0 ]
then
    echo "An error occured during creation of the Bastion"
    exit 1
else
    echo "Bastion successfully created"
fi


# Create NSG for activate SSH

create_NSG "$RG_NAME" "$BASTION_NAME" "$NSG" 

# Checking if the creation is a success
if [ $? -eq 0 ]
then
    echo "An error occured during creation of the NSG"
    exit 1
else
    echo "NSG successfully created"
fi


# Create rule for SSH

create_rule_ssh "$RG_NAME" "$NSG"

# Checking if the creation is a success
if [ $? -eq 0 ]
then
    echo "An error occured during creation the rule SSH"
    exit 1
else
    echo "Rule SSH created"
fi


# Add rule SSH for the bastion

add_rule_interface "$RG_NAME" "$BASTION_NAME" "$NSG"

# Checking if the creation is a success
if [ $? -eq 0 ]
then
    echo "An error occured during add the rule SSH"
    exit 1
else
    echo "Rule SSH add succed"
fi


# Searching for the public IP of the Bastine

find_ip_public "$RG_NAME" "$BASTION_NAME"