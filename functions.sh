#!/bin/bash

#include the variables
source .env


check_resource_group_exists() {
    local RESOURCE_GROUP_NAME=$1

    GROUP_EXISTS=$(az group exists --name $RESOURCE_GROUP_NAME)

    if [ "$GROUP_EXISTS" = "true" ]; then
        return 0
    else
        return 1
    fi

}


create_resource_group () {
    local RESOURCE_GROUP_NAME=$1
    local LOCATION=$2

    CREATE_RESOURCE_GROUP=$(az group create  \
        --name $RESOURCE_GROUP_NAME  \
        --location $LOCATION)

    if [ "$CREATE_RESOURCE_GROUP" = "true" ]; then
        return 0
    else
        return 1
    fi

}


create_vnet () {
    local VNET_NAME=$1
    local RESOURCE_GROUP_NAME=$2
    local NETWORK=$3
    local SUBNET_NETWORK=$4
    local SUBNET=$5

    CREATE_VNET=$(az network vnet create \
        --name $VNET_NAME \
        --resource-group $RESOURCE_GROUP_NAME \
        --address-prefix $NETWORK \
        --subnet-name $SUBNET \
        --subnet-prefixes $SUBNET_NETWORK)

    if [ "$CREATE_VNET" = "true" ]; then
        return 0
    else
        return 1
    fi
}

create_public_ip() {
    local RESOURCE_GROUP_NAME=$1
    local NAME_PUBLIC_IP=$2
    local LOCATION=$3

    
    CREATION_IP=$(az network public-ip create \
        --resource-group $RESOURCE_GROUP_NAME \
        --name $NAME_PUBLIC_IP \
        --sku Standard \
        --location $LOCATION)

    if [ "$CREATION_IP" = "true" ]; then
        return 0
    else
        return 1
    fi
}


create_nic() {
    local RESOURCE_GROUP_NAME=$1
    local NAME_NIC=$2
    local NAME_VNET=$3
    local NAME_SUBNET=$4
    local NAME_PUBLIC_IP=$5
    local PRIVATE_IP_ADDRESS=$6


    CREATION_CARTE=$(az network nic create \
        --resource-group $RESOURCE_GROUP_NAME \
        --name $NAME_NIC \
        --vnet-name $NAME_VNET \
        --subnet $NAME_SUBNET \
        --public-ip-address $NAME_PUBLIC_IP \
        --private-ip-address $PRIVATE_IP_ADDRESS)

    if [ "$CREATION_CARTE" = "true" ]; then
        return 0
    else
        return 1
    fi
}


create_bastion () {
    local RESOURCE_GROUP_NAME=$1
    local VM_NAME=$2
    local VM_IMAGE=$3
    local ADMIN_USERNAME=$4
    local SSH_PUBLIC_KEY=$5
    local NAME_NIC=$6
    local SIZE=$7



    VM_CREATION_OUTPUT=$(az vm create \
        --resource-group $RESOURCE_GROUP_NAME \
        --name $VM_NAME \
        --nics $NAME_NIC \
        --image $VM_IMAGE \
        --admin-username $ADMIN_USERNAME  \
        --ssh-key-value "$SSH_PUBLIC_KEY" \
        --size $SIZE)

    if [ "$VM_CREATION_OUTPUT" = "true" ]; then
        return 0
    else
        return 1
    fi

}

find_ip_public () {
    local RESOURCE_GROUP_NAME=$1
    local VM_NAME=$2

    IP_ADDRESS=$(az vm show \
        --show-details \
        --resource-group $RESOURCE_GROUP_NAME \
        --name $VM_NAME \
        --query publicIps \
        --output tsv)

    if [ -z "$IP_ADDRESS" ]; then
        echo "Not found IP for the VM $VM_NAME : $IP_ADDRESS"
        return 0
    else
        echo "IP Public for the VM $VM_NAME : $IP_ADDRESS"
        return 1
    fi

}

create_NSG() {
    local RESOURCE_GROUP_NAME=$1
    local VM_NAME=$2
    local MyNSG=$3

    ZONE=$(az vm show \
        --resource-group $RESOURCE_GROUP_NAME \
        --name $VM_NAME \
        --query "location" \
        -o tsv)


    CREATE_NSG=$(az network nsg create \
        --name $MyNSG \
        --resource-group $RESOURCE_GROUP_NAME \
        --location $ZONE)

    if [ "$CREATE_NSG" = "true" ]; then
        return 0
    else
        return 1
    fi

}


create_rule_ssh () {
    local RESOURCE_GROUP_NAME=$1
    local MyNSG=$2

    CREATE_RULE=$(az network nsg rule create \
    --resource-group $RESOURCE_GROUP_NAME \
    --nsg-name $MyNSG \
    --name AllowSSH \
    --protocol Tcp \
    --direction Inbound \
    --priority 1000 \
    --source-address-prefix '*' \
    --source-port-range '*' \
    --destination-address-prefix '*' \
    --destination-port-range 22 \
    --access Allow)

    if [ "$CREATE_RULE" = "true" ]; then
        return 0
    else
        return 1
    fi
}


add_rule_interface () {
    local RESOURCE_GROUP_NAME=$1
    local VM_NAME=$2
    local MyNSG=$3

    NIC_ID=$(az vm show \
    --resource-group $RESOURCE_GROUP_NAME \
    --name $VM_NAME \
    --query "networkProfile.networkInterfaces[0].id" \
    -o tsv)


    ADD_RULE=$(az network nic update \
    --ids $NIC_ID \
    --network-security-group $MyNSG)

    if [ "$ADD_RULE" = "true" ]; then
        return 0
    else
        return 1
    fi
}


