#!/bin/bash

# Script to remove resource group

az group delete --name "$1" \
--yes --no-wait

# Checking if the creation is a success
if [ $? -eq 1 ]
then
    echo "An error occured during delete ressource group"
    exit 1
else
    echo "ressource group delete succed"
fi
